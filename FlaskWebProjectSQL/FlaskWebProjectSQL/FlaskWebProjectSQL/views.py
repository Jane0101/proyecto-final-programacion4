"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template
from FlaskWebProjectSQL import app
import pypyodbc
import sys
import json
import requests

Connection = "DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456"

@app.route('/')
@app.route('/home')
def home():
    return render_template('index.html',
        title='Home Page',
        year=datetime.now().year,)

@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template('contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.')

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template('about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.')

@app.route('/api/SaveEncuenta', methods=['POST'])
def Save_Encuenta():
    req_data = request.get_json()
    #item = req_data['Nombre']
    #print(req_data,'*******',request.form.get('Nombre'))
    result = {'Exito':True,'Mensaje':'Encuenta guardada con exito.','Valor':0}
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("insert into Encuestas(nombre,fecha) values(?,?)", (request.form.get('Nombre'),datetime.now()))
    myCursor.execute("SELECT @@IDENTITY AS ID;")
    #print('Id of inserted record is:{}'.format(myCursor.fetchone()[0]))
    #row = myCursor.fetchone()
    seed_id = myCursor.fetchone()[0]
    #print("Conectado ....\n",seed_id)
    #result['Valor'] = seed_id
    myCursor.commit()
    con2.close()
    return jsonify({'result': {'Exito':True,'Mensaje':'Encuenta guardada con exito.', 'Valor':str(seed_id)}})

@app.route('/Agregar_Encuestas')
def Agregar_Encuesta():
    return render_template('AgregarEncuesta.html')

@app.route('/api/SavePregunta', methods=['POST'])
def Save_Pregunta():
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    pre = json.loads(request.form.get('Preguntas'))
    res = json.loads(request.form.get('Respuestas'))
    for idx, val in enumerate(pre):
        myCursor.execute("insert into Preguntas(Id_Encuenta,Pregunta,Respuesta) values(?,?,?)", (request.form.get('Id'),val,res[idx]))
        myCursor.commit()

    con2.close()
    return jsonify({'result': {'Exito':True,'Mensaje':'Pregunta(s) guardada(s) con exito.'}})

@app.route('/Ver_Encuestas')
def Ver_Encuesta():
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("SELECT *from Encuestas")
    data = myCursor.fetchall()
    con2.close()
    return render_template('VerEncuesta.html',Data=data)

@app.route('/Empleados')
def Empleados():
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("SELECT *from Empleados where Estado=1")
    data = myCursor.fetchall()
    con2.close()
    return render_template('Empleados.html',Data=data,title='Listado de Empleados')

@app.route('/CrearEvaluacion')
def Crear_Evaluacion():
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("SELECT *from Encuestas")
    dataE = myCursor.fetchall()
    myCursor.execute("SELECT *from Empleados")
    dataEm = myCursor.fetchall()
    con2.close()
    return render_template('CrearEvaluacion.html',DataEn=dataE,DataEm=dataEm,title='Evaluacion Empleado',year=datetime.now().year)

@app.route('/VerEvaluacion')
def Ver_Evaluacion():
    con2 = pypyodbc.connect(Connection)
    myCursor = con2.cursor()
    myCursor.execute("SELECT DISTINCT ee.Id_Encuenta, e.Nombre, FORMAT (ee.Fecha, 'dd/MM/yyyy ') AS Fecha FROM Empleado_Encuesta ee, Encuestas e WHERE e.Id_Encuenta = ee.Id_Encuenta ORDER BY Fecha DESC")
    dataE = myCursor.fetchall()
    con2.close()
    return render_template('VerEvaluacion.html',Data=dataE,title='Ver Evaluaciones',year=datetime.now().year)

@app.route('/Conversor')
def Ver_Conversor():
    response = requests.get("https://fcsapi.com/api-v2/forex/list?type=forex&&access_key=6Wlati0AGt6yOtKYoc6n71LtRsN51w65POoO3BEggB2oUYdnko")
    data=[]
    if response.status_code==200:
        data=response.json()['response']
    return render_template('Conversor.html',title='Convertir Monedas',year=datetime.now().year, Lista=data)

########################  APIS ########################
@app.route('/api/EditEmpleado', methods=['POST'])
def Editar_Empleado():
    result = {'Exito':True,'Mensaje':'Encuenta guardada con exito.','Valor':0}
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("update Empleados set Nombre=?,Apellido=?,Sueldo=? where Id=?", (request.form.get('Nombre'),request.form.get('Apellido'),request.form.get('Sueldo'),request.form.get('Id')))
    myCursor.commit()
    con2.close()
    return jsonify({'result': {'Exito':True,'Mensaje':'Empleado Actualizado con exito.'}})

@app.route('/api/DeleteEmpleado', methods=['POST'])
def Eliminar_Empleado():
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("update Empleados set Estado=? where Id=?", (0,request.form.get('Id')))
    myCursor.commit()
    con2.close()
    return jsonify({'result': {'Exito':True,'Mensaje':'Empleado Eliminado con exito.'}})

@app.route('/api/AddEmpleado', methods=['POST'])
def Agregar_Empleado():
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("insert into Empleados(Apellido, Nombre, Sueldo, Estado, Fecha_Ingreso) values(?,?,?,?,?)", (request.form.get('Apellido'),request.form.get('Nombre'),request.form.get('Sueldo'),1,datetime.now()))
    myCursor.commit()
    con2.close()
    return jsonify({'result': {'Exito':True,'Mensaje':'Empleado Agregado con exito.'}})

@app.route('/api/ShowEncuesta', methods=['POST'])
def Mostrar_Encuesta():
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("select *from Preguntas where Id_Encuenta=" + request.form.get('Id'))
    data = myCursor.fetchall()
    con2.close()
    return jsonify({'result': {'Exito':True,'Valor':data}})

@app.route('/api/AddEvaluation', methods=['POST'])
def Agregar_Evaluacion():
    con2 = pypyodbc.connect("DRIVER={SQL Server};SERVER=DESKTOP-62CJJS5\SQLEXPRESS;DATABASE=Evalueacion;UID=sa;PWD=123456")
    myCursor = con2.cursor()
    myCursor.execute("INSERT INTO Empleado_Encuesta(Id_Empleado,Id_Encuenta,Id_Pregunta,Respuesta,Peso,Fecha) VALUES(?,?,?,?,?,?)", (request.form.get('Id_Empleado'),request.form.get('Id_Encuenta'),request.form.get('Id_Pregunta'),request.form.get('Respuesta'),request.form.get('Peso'),datetime.now()))
    myCursor.commit()
    con2.close()
    return jsonify({'result': {'Exito':True}})

@app.route('/api/ShowEvaluacion', methods=['POST'])
def Mostrar_Evaluacion():
    con2 = pypyodbc.connect(Connection)
    myCursor = con2.cursor()
    myCursor.execute("SELECT (e.Nombre + ' ' + e.Apellido) AS NombreEmpleado, ROUND(SUM(ee.Peso), 2) Total, e.Sueldo FROM Empleado_Encuesta ee, Empleados e WHERE ee.Id_Encuenta = ? AND FORMAT (ee.Fecha, 'dd/MM/yyyy') = ? AND e.Id = ee.Id_Empleado GROUP BY Nombre, e.Apellido, Sueldo",(request.form.get('Id'),request.form.get('Fecha')))
    data = myCursor.fetchall()
    con2.close()
    return jsonify({'result': {'Exito':True,'Valor':data}})

@app.route('/api/ConvertCurrency', methods=['POST'])
def Convert_Currency():
    response = requests.get("https://fcsapi.com/api-v2/forex/converter?symbol=" + request.form.get('Symbol') + "&amount=" + request.form.get('Amount') + "&access_key=6Wlati0AGt6yOtKYoc6n71LtRsN51w65POoO3BEggB2oUYdnko")
    if response.status_code==200:
        return jsonify({'result': {'Exito':True,'Valor':response.json()['response']}})
    else:
        return jsonify({'result': {'Exito':False,'Mensaje':response.msg}})
